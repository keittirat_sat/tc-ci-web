<?php

class Generate extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $wid = $this->input->get('wid');
        $wid = is_numeric($wid) ? $wid : 1;
        $weight = $this->dam->get_weight();
        $this->template->set('all_weight', $weight);
        $re_weight = array();
        foreach ($weight as $each_weight) {
            $re_weight[$each_weight->wid] = $each_weight;
        }
        $weight = $re_weight[$wid];
        $convert_list_weight = json_decode($weight->wid_raws);
        $this->template->set('weight', $convert_list_weight);
        $this->template->set('wid_raws', $weight->wid_raws);
        $this->template->set('wid', $wid);
        $this->template->set('title', $weight->wid_name . ' | ปรับแต่งค่าน้ำหนัก CI');


        $list_menu = $this->dam->get_list_menu();
        $list_menu = re_list_menu($list_menu);
        $this->template->set('list_menu', $list_menu);
        $this->template->load();
    }

}
