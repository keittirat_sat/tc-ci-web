<?php

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
    }

    public function create_profile() {
        $post = $this->input->post();
        if ($this->dam->create_profile($post)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function update_profile() {
        $post = $this->input->post();
        $wid = $post['wid'];
        unset($post['wid']);
        if ($this->dam->update_profile($post, $wid)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

}
