<?php

function re_list_menu($list_menu) {
    $list_menu_arr = array();
    foreach ($list_menu as $each_menu) {
        if (!array_key_exists($each_menu->parent, $list_menu_arr)) {
            $list_menu_arr[$each_menu->parent] = array();
        }
        array_push($list_menu_arr[$each_menu->parent], $each_menu);
    }
    return $list_menu_arr;
}
