<?php

function gen_table($list_menu, $index, $weight, $order) { ?>
    <?php if (array_key_exists($index, $list_menu)): ?>
        <?php foreach ($list_menu[$index] as $each_element): ?>
            <?php $this_order = $order == '' ? $each_element->order : "{$order}.{$each_element->order}"; ?>
            <div class="form-group" id="element_<?php echo str_replace('.', '_', $this_order) ?>">
                <div class="panel panel-default block_<?php echo $each_element->list_id ?>">
                    <div class="panel-heading">
                        <?php echo $this_order . " " . trim($each_element->info) ?>
                    </div>
                    <div class="panel-body">
                        <p><input type="text" class="form-control" data-parent="<?php echo $each_element->parent ?>"  value="<?php echo $each_element->weight; ?>"></p>
                        <?php $list_id = $each_element->list_id ?>
                        <?php if (isset($weight->$list_id)): ?>
                            <label>ช่องกรอกย่อยดูเทียบเอกสาร <span class="badge" id="sum_<?php echo $each_element->list_id ?>">Calculating...</span></label>
                            <div class="form-horizontal">
                                <?php foreach ($weight->$list_id as $sub_index => $each_weight): ?>
                                    <div class="form-group">
                                        <label class="control-label col-xs-1"><?php echo $sub_index + 1 ?></label>
                                        <div class="col-xs-11">
                                            <input type="text" data-parent="<?php echo $list_id ?>" value="<?php echo $each_weight; ?>" class="form-control part_element">
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php gen_table($list_menu, $each_element->list_id, $weight, $this_order) ?>
        <?php endforeach; ?>
    <?php endif; ?>
<?php } ?>

<?php

function gen_nav($list_menu, $index, $order) { ?> 
    <?php if (array_key_exists($index, $list_menu)): ?>
        <?php foreach ($list_menu[$index] as $each_element): ?>
            <?php $this_order = $order == '' ? $each_element->order : "{$order}.{$each_element->order}"; ?>
            <li>
                <a href="#element_<?php echo str_replace('.', '_', $this_order) ?>">
                    <?php echo $this_order . " " . trim($each_element->info) ?>
                </a>
                <ul class="nav">
                    <?php gen_nav($list_menu, $each_element->list_id, $this_order) ?>
                </ul>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
<?php } ?>

<div class="row" id="part_panel">
    <div class="col-xs-4 list_element">
        <div class="affix-top panel_nav">
            <nav class="navbar navbar-default" style='margin-bottom: 10px;'>
                <div class="container-fluid">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle navbar-btn" data-toggle="dropdown">
                            <i class='glyphicon glyphicon-cog'></i>
                            แสดงค่าน้ำหนัก
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php foreach ($all_weight as $each_wight): ?>
                                <li <?php echo $wid == $each_wight->wid ? "class='active'" : ""; ?>><a href="<?php echo "?wid={$each_wight->wid}"; ?>"><?php echo $each_wight->wid_name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle navbar-btn" data-toggle="dropdown">
                            <i class='glyphicon glyphicon-screenshot'></i>
                            จัดการ
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">                           
                            <li><a id="save_btn" href='#export_area'><i class="glyphicon glyphicon-export"></i>&nbsp;ส่งออก</a></li>
                            <li><a id="update_btn" href='#export_area'><i class="glyphicon glyphicon-ok"></i>&nbsp;อัพเดต</a></li>
                            <li><a id="save_as_btn" href='#export_area'><i class="glyphicon glyphicon-save"></i>&nbsp;บันทึกเป็นชุดใหม่</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <ul class="nav list_element_part">
                <?php gen_nav($list_menu, 0, '') ?>
                <li  style="height: 1px; margin: 9px 0; overflow: hidden; background-color: #e5e5e5;"></li>
                <li><a href="<?php echo site_url("program/Setup.exe"); ?>"><i class="glyphicon glyphicon-download"></i>&nbsp;CI Original</a></li>
                <li><a href="<?php echo site_url("program/Setup_WA.exe"); ?>"><i class="glyphicon glyphicon-download"></i>&nbsp;CI Weight Adjustable</a></li>
                <li><a href="<?php echo site_url("program/ci_v4.00.xls"); ?>"><i class="glyphicon glyphicon-download"></i>&nbsp;CI v.4.00 Excel file</a></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-8">
        <?php gen_table($list_menu, 0, $weight, ''); ?>
        <form role="form" method="post" id="export_form">
            <div class="panel panel-success">
                <div class="panel-heading"><i class="glyphicon glyphicon-export"></i>&nbsp;Export</div>
                <div class="panel-body">
                    <textarea class="form-control" readonly="" id="export_area" name="wid_raws" rows="12"><?php echo $wid_raws; ?></textarea>
                    <input type="hidden" value="<?php echo $wid ?>" name="wid" id="wid">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="save_as_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">กรุณากรอกชื่อ Profile</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">ตรวจสอบพบค่า Error ในการใส่ค่าคำนวณ ถ้าต้องการดำเนิการต่อให้กดบันทึก หรือกดยกเลิกเพื่อย้อนกลับไปแก้ไข</div>
                <div class="alert alert-warning">ชื่อ Profile ไม่สามารถเว้นว่างได้</div>
                <div class="form-group" style="margin-bottom: 0px;"> 
                    <input type="text" placeholder="ชื่อ Profile" class="form-control" id="profile_name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-primary" id="save_as_btn_exceute" data-loading-text="กำลังบันทึก...">บันทึก</button>
            </div>
        </div>
    </div>
</div>

<style>
    .panel_nav{
        top: 0px;
        z-index: 99;
    }

    .list_element_part{
        font-size: 12px !important;
        width: 293px;
    }

    .list_element_part li a{        
        border-right: 2px solid;
        border-color: transparent;
        color: #222;
    }

    .list_element_part li.active>a{
        border-color: #6699ff;
        font-weight: bold;
    }

    .list_element_part li>a:hover{
        border-color: #ff0000;
    }

    .list_element_part li.active li.active>a{
        border-color: #99ccff;
    }

    .list_element_part.nav .nav{
        padding-left: 15px;
        display: none;
    }

    .list_element_part.nav li.active>.nav{
        display: block;
    }
</style>

<script>
    var error_flag;
    function built_object() {
        var json = {};
        $.each($('.part_element'), function(idx, element) {
            var parent = $(element).attr('data-parent');
            var val = Number($(element).val());
            if (!Array.isArray(json[parent])) {
                json[parent] = [];
            }
            json[parent].push(val);
        });
        return json;
    }

    function check_val() {
        error_flag = false;
        var json = built_object();
        $.each(json, function(idx, ele) {
            var w = 1;
            $.each(ele, function(input_block, val) {
                w -= val;
            });

            if (Math.abs(w) > 0.00001) {
                $('.part_element[data-parent=' + idx + ']').parents('.form-group').addClass('has-error');
                $('.block_' + idx).removeClass('panel-default').addClass('panel-danger');
                $('#sum_' + parent).text(w.toFixed(5));
                error_flag = true;
            } else {
                $('.part_element[data-parent=' + idx + ']').parents('.form-group').removeClass('has-error');
                $('.block_' + idx).removeClass('panel-danger').addClass('panel-default');
                $('#sum_' + idx).text("0.00");
            }
        });
        return json;
    }

    $(function() {
        $(window).bind('scroll', function(e) {
            var offset_top = 106;
            var scrolledY = $(window).scrollTop();
            var cal_offset = scrolledY - offset_top;
            if (cal_offset < 0) {
                cal_offset = 0;
            }

            if (cal_offset > 0) {
                if ($('.panel_nav').is('.affix-top')) {
                    $('.panel_nav').removeClass('affix-top').addClass('affix');
                    check_val();
                }
            } else {
                if ($('.panel_nav').is('.affix')) {
                    $('.panel_nav').removeClass('affix').addClass('affix-top');
                    check_val();
                }
            }
        });

//        built_object();

        $('#save_btn').click(function() {
            var json = built_object();
            $('#export_area').val(JSON.stringify(json));
        });

        $('#export_area').click(function() {
            $(this).select();
        });

        $('#save_as_btn').click(function() {
            var json = check_val();
            $('#export_area').val(JSON.stringify(json));
            if (error_flag) {
                $('.alert-danger').show();
            } else {
                $('.alert-danger').hide();
            }
            $('.alert-warning').hide();
            $('#save_as_form').modal();
        });

        $('.part_element').each(function(idx, ele) {
            $(ele).keyup(function() {
                var parent = $(this).attr('data-parent');
                var w = 1;
                $('.part_element[data-parent=' + parent + ']').each(function(i, v) {
                    var current_val = Number($(v).val());
                    w -= current_val;
                });

                if (Math.abs(w) > 0.00001) {
                    $('.part_element[data-parent=' + parent + ']').parents('.form-group').addClass('has-error');
                    $('#sum_' + parent).text(w.toFixed(5));
                } else {
                    $('.part_element[data-parent=' + parent + ']').parents('.form-group').removeClass('has-error');
                    $('#sum_' + parent).text("0.00");
                }
            });
        });

        $('#save_as_btn_exceute').click(function() {
            var profile_name = $('#profile_name').val();
            if ($.trim(profile_name)) {
                $(this).button('loading');
                $.post('<?php echo site_url('api/create_profile'); ?>', {wid_name: profile_name, wid_raws: $('#export_area').val()}, function(res) {
                    if (res.status === "success") {
                        $('#save_as_btn_exceute').text('บันทึกสำเร็จ');
                        $('#save_as_form').modal('hide');
                    } else {
                        $('#save_as_btn_exceute').button('reset');
                        $('#save_as_form').modal('hide');
                        alert("Cannot update");
                    }
                }, 'json');
            } else {
                $('.alert-warning').show();
            }
        });

        $('#update_btn').click(function() {
            var json = check_val();
            $('#export_area').val(JSON.stringify(json));

            var wid = $('#wid').val();
            var wid_raws = $('#export_area').val();

            console.log(wid, wid_raws);

            $.post('<?php echo site_url('api/update_profile'); ?>', {wid: wid, wid_raws: wid_raws}, function(res) {
                if (res.status === "success") {
                    alert('อัพเดต Profile สำเร็จ');
                } else {
                    alert("update fail");
                }
            }, 'json');
        });
    });
</script>