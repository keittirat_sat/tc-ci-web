<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php echo isset($title) ? $title : "สำนักกรมชลประทาน" ?> | หน่วยรักษาความปลอดภัยเขื่อน</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--CSS-->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">        

        <!--JS-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> 
        <?php echo js_asset("jquery.form.js"); ?>

        <!--Overide CSS-->
        <style>
            /*Responsive disabled*/
            body{
                min-width: 970px;
            }

            .container {
                max-width: none !important;
                width: 970px;
            }
        </style>
    </head>
    <body data-spy="scroll" data-target=".list_element">
        <div class="container">
            <div class="page-header">
                <h3><i class="glyphicon glyphicon-wrench"></i>&nbsp;ระบบปรับแต่งค่าน้ำหนักการประเมินค่าความเสื่อมสภาพ (CI) <small>หน่วยรักษาความปลอดภัยเขื่อน</small></h3>
            </div>
            <?php echo $contents; ?>
        </div>
    </body>
</html>
