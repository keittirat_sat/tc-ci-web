<?php

class Dam extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function get_weight($wid = 0) {
        $this->ci->db->from('weight');
        if ($wid != 0) {
            $this->ci->db->where('wid', $wid);
        }
        return $this->ci->db->get()->result();
    }

    public function get_list_menu() {
        return $this->ci->db->from('list_menu')->order_by('order', 'asc')->get()->result();
    }

    public function create_profile($post) {
        return $this->ci->db->insert('weight', $post);
    }

    public function update_profile($post, $post_id) {
        return $this->ci->db->where('wid', $post_id)->update('weight', $post);
    }

}
